# probability of each element in sample
a1<-.05
b1<- .07
c1<-(1-(a1+b1))/4
d1<-(1-(a1+b1))/4
e1<-(1-(a1+b1))/4
p<-c(a1,b1,c1,d1,e1)
A=sample(e,403,T,prob = p)
 b=matrix(0,403,20)
for(i in 1:20){
 repeat {
    A=sample(e,403,T,prob = p)
    c(print(mean(A)),print(var(A)))
    
    if (mean(A)>3.7 & var(A)<1.1){
      b[,i]=A
      break
      
    }
 }}
 b
 apply(b, 2, mean)
 apply(b, 2, var)
